package server;

import com.zitga.core.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class ChatServerApp {
//    private int port;
//
//    public ChatServerApp(int port){
//        this.port=port;
//    }

    public static void run(){
//        NioEventLoopGroup bossGroup=new NioEventLoopGroup();
//        NioEventLoopGroup workerGroup=new NioEventLoopGroup();

        try{
            ServerBootstrap.start();
//            ServerBootstrap serverBootstrap=new ServerBootstrap();
//            serverBootstrap.group(bossGroup,workerGroup).channel(NioServerSocketChannel.class)
//                    .childHandler(new ChannelInitializer<SocketChannel>() {
//                        @Override
//                        protected void initChannel(SocketChannel socketChannel) throws Exception {
//                            socketChannel.pipeline().addLast(new ChatServerHandler());
//                        }
//                    }).option(ChannelOption.SO_BACKLOG,128)
//                    .childOption(ChannelOption.SO_KEEPALIVE,true);
//
//            ChannelFuture channelFuture=serverBootstrap.bind(port).sync();
//            channelFuture.channel().closeFuture().sync();

        }  catch(Exception e){
            ServerBootstrap.stop();
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
         ChatServerApp.run();
    }
}
