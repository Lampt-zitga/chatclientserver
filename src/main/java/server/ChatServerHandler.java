package server;

import com.zitga.core.annotation.socket.SocketHandler;
import com.zitga.core.handler.socket.AbstractSocketHandler;
import com.zitga.core.handler.socket.support.context.HandlerContext;
import com.zitga.core.utils.socket.SerializeHelper;
import com.zitga.core.utils.socket.SocketUtils;
import constrant.OpCode;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.net.SocketAddress;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@SocketHandler(OpCode.BROADCAST_MESSAGE_SERVER)
public class ChatServerHandler extends AbstractSocketHandler {

    @Override
    public void handle(HandlerContext handlerContext, int opCode, ByteBuf byteBuf, boolean isTcp) {
        System.out.println("Receive from:" + handlerContext.getPeer().getId());
        String mess = SerializeHelper.readString(byteBuf);
        System.out.println("mess: " + mess);
    }
}







//    public static ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
//    public static Map<SocketAddress, Integer> listSk = new HashMap<>();
//    public static Integer socketNID = 1;
//    public static int socketCount = 1;

//    @Override
//    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//        Channel incomming=ctx.channel();
//        System.out.println("receive from "+incomming.remoteAddress());
//        int icmChannelId=listSk.get(incomming.remoteAddress());
//        ByteBuf byteBuf=(ByteBuf) msg;
//        String s=byteBuf.toString(Charset.forName("UTF-8"));
//        char[] cs=(byteBuf.toString(Charset.forName("UTF-8"))).toCharArray();
//        if(cs.length>100){
//            String erro="must under 100 characters, please type again!";
//            incomming.writeAndFlush(Unpooled.copiedBuffer(erro,Charset.forName("UTF-8")));
//        }else{
//            for(Channel channel:channels){
//                int channelId=listSk.get(channel.remoteAddress());
//                if(channelId!=icmChannelId){
//                    System.out.println("send to "+channel.remoteAddress());
//                    channel.writeAndFlush(Unpooled.copiedBuffer(s,Charset.forName("UTF-8")));
//                }
//            }
//        }
//    }
//
//    @Override
//    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
//        System.out.println("server: handler added");
//        Channel incomming=ctx.channel();
//        if(socketCount%2==1){
//            listSk.put(incomming.remoteAddress(),socketNID);
//            socketCount+=1;
//        }else{
//            listSk.put(incomming.remoteAddress(), socketNID);
//            socketCount+=1;
//            socketNID+=1;
//        }
//        String msg="<"+incomming.remoteAddress()+"> joined";
//        channels.writeAndFlush(Unpooled.copiedBuffer(msg,Charset.forName("UTF-8")));
//        channels.add(ctx.channel());
//    }
//
//    @Override
//    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
//        System.out.println("server: handler removed");
//        Channel incomming=ctx.channel();
//        String msg="<"+incomming.remoteAddress()+"> left\r\n";
//        channels.writeAndFlush(Unpooled.copiedBuffer(msg,Charset.forName("UTF-8")));
//    }
//
//    @Override
//    public void channelActive(ChannelHandlerContext ctx) throws Exception {
//        System.out.println("server: channel active");
//        Channel channel= ctx.channel();
//        System.out.println("Channel "+channel.remoteAddress()+" active");
//
//    }
//
//    @Override
//    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
//        System.out.println("server: channel inactive");
//        Channel channel= ctx.channel();
//        System.out.println("Channel "+channel.remoteAddress()+" inactive");
//    }
//
//    @Override
//    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
//        System.out.println("exception");
////        cause.printStackTrace();
//        ctx.close();
//    }


