package constrant;

import com.zitga.core.constants.socket.BaseOpCode;

public class OpCode extends BaseOpCode {
    public static final int SEND_MESSAGE_CLIENT =10;
    public static final int RECEIVE_MESSAGE_CLIENT =11;
    public static final int BROADCAST_MESSAGE_SERVER =12;
}
