package client.bootstraps;

import client.handler.DelivererHandler;
import client.handler.ServerActionListioner;
import com.zitga.bean.annotation.BeanField;
import com.zitga.core.config.ServerConfig;
import com.zitga.core.utils.socket.SocketUtils;
import com.zitga.core.utils.socket.tcpClient.TcpClientConfig;
import com.zitga.core.utils.socket.tcpClient.TcpClientHandler;
import com.zitga.utils.RandomUtils;
import constrant.OpCode;
import io.netty.buffer.ByteBuf;

public class DelivererBootstrap {
//    private String host;
//    private int port;
//
//    public DelivererBootstrap(String host, int port) {
//        this.host = host;
//        this.port = port;
//    }
//        NioEventLoopGroup loopGroup=new NioEventLoopGroup();
//
//
//            Bootstrap cbt=new Bootstrap();
//            cbt.group(loopGroup).channel(NioSocketChannel.class)
//                    .handler(new ChannelInitializer<SocketChannel>() {
//                        protected void initChannel(SocketChannel socketChannel) throws Exception {
//                            socketChannel.pipeline()
//                                    .addLast(new DelivererHandler(username));
//                        }
//                    }).option(ChannelOption.SO_KEEPALIVE,true);
//
//            ChannelFuture cf=cbt.connect(host,port).sync();
//            cf.channel().closeFuture().sync();
    private DelivererHandler handler;

    @BeanField
    private ServerConfig serverConfig;

    private TcpClientConfig clientConfig;

    private void init(String username){
        clientConfig=new TcpClientConfig();
        handler=new DelivererHandler(username);
        handler.registerListener(new ServerActionListioner());
    }

    public void run(String username){

        try{
            init(username);
            if(!handler.isConnected()){
                connectToChatServer();
            }else{
                ping();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void connectToChatServer(){
        handler.connectSync("localhost",8100);
    }
    public void ping() {
        ByteBuf out = SocketUtils.createByteBuf(OpCode.BROADCAST_MESSAGE_SERVER);
        out.writeByte(1);
        out.writeByte(RandomUtils.nextInt(100));
        if (handler.isConnected()) {
            handler.getPeer().send(out);
        } else {
            SocketUtils.release(out);
        }
    }
}
