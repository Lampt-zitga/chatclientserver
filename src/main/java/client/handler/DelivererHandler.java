package client.handler;

import com.zitga.core.utils.socket.tcpClient.TcpClientHandler;
import constrant.OpCode;
import com.zitga.core.annotation.socket.SocketHandler;
import com.zitga.core.handler.socket.AbstractSocketHandler;
import com.zitga.core.handler.socket.support.context.HandlerContext;
import com.zitga.core.utils.socket.SocketUtils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;

import java.nio.charset.Charset;
import java.util.Scanner;

@SocketHandler(OpCode.SEND_MESSAGE_CLIENT)
public class DelivererHandler  extends TcpClientHandler{
    //    ChannelInboundHandlerAdapter
    private String user_name;

    public DelivererHandler(String username) {
        super();
        this.user_name = username;
    }

    @Override
    public void channelRead0(ChannelHandlerContext context, ByteBuf in) throws Exception {
        Scanner sc = new Scanner(System.in);
        String mess = sc.nextLine();
        if (mess.length() > 100) {
            System.out.println("message is too long, must under 100 characters!");
        } else {
            mess="<" + user_name + ">: "+mess;
            try {
                ByteBuf out = SocketUtils.createByteBuf(OpCode.BROADCAST_MESSAGE_SERVER);
                out.writeCharSequence(mess, Charset.forName("UTF-8"));
                super.channelRead0(context, out);
            } catch (Exception e) {
                System.out.println("send fail");
            }
        }


    }
//    @Override
//    public void handle(HandlerContext handlerContext, int opCode, ByteBuf byteBuf, boolean isTcp) {
//        while (true) {
//            System.out.println("begin");
//            Scanner sc = new Scanner(System.in);
//            String mess = sc.nextLine();
//            if (mess.length() > 100) {
//                System.out.println("message is too long, must under 100 characters!");
//            } else {
//                ByteBuf byteBuf1= Unpooled.copiedBuffer(mess, Charset.forName("UTF-8"));
//                mess="<" + user_name + ">: "+mess;
//                try {
//                    ByteBuf out = SocketUtils.createByteBuf(OpCode.BROADCAST_MESSAGE_SERVER);
//                    out.writeCharSequence(mess, Charset.forName("UTF-8"));
//                    handlerContext.getPeer().send(out);
//                } catch (Exception e) {
//                    System.out.println("send fail");
//                }
//            }
//        }
//    }

//    @Override
//    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//        while(true){
//            Scanner sc=new Scanner(System.in);
//            String mess="<"+user_name+">: "+sc.nextLine();
//            char[] c=mess.toCharArray();
//            if(c.length>100){
//                System.out.println("message is too long, must under 100 characters!");
//            }else{
//                ByteBuf byteBuf1= Unpooled.copiedBuffer(mess, Charset.forName("UTF-8"));
//                ChannelFuture future=ctx.writeAndFlush(byteBuf1);
//            }
//        }
//    }
//
//
//
//    @Override
//    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
//        cause.printStackTrace();
//        ctx.close();
//    }
}
