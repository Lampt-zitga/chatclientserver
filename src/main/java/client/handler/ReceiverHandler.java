package client.handler;

import constrant.OpCode;
import com.zitga.core.annotation.socket.SocketHandler;
import com.zitga.core.handler.socket.AbstractSocketHandler;
import com.zitga.core.handler.socket.support.context.HandlerContext;
import com.zitga.core.utils.socket.SerializeHelper;
import io.netty.buffer.ByteBuf;


@SocketHandler(OpCode.RECEIVE_MESSAGE_CLIENT)
public class ReceiverHandler extends AbstractSocketHandler {
//    @Override
//    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//        ByteBuf byteBuf2=(ByteBuf) msg;
//        System.out.println(byteBuf2.toString(Charset.forName("UTF-8")));
//    }

    @Override
    public void handle(HandlerContext handlerContext, int opCode, ByteBuf byteBuf, boolean isTcp) {
        String msg= SerializeHelper.readString(byteBuf);
        System.out.println(msg);
    }
//    @Override
//    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
//        cause.printStackTrace();
//        ctx.close();
//    }
}
