package client.handler;

import com.zitga.bean.BeanContainer;
import com.zitga.bean.annotation.BeanField;
import com.zitga.core.handler.socket.AbstractSocketHandler;
import com.zitga.core.handler.socket.support.context.HandlerContext;
import com.zitga.core.handler.socket.support.context.HandlerContextManager;
import com.zitga.core.handler.socket.support.context.Peer;
import com.zitga.core.registry.socket.SocketHandlerRegistry;
import com.zitga.core.utils.socket.SocketUtils;
import com.zitga.core.utils.socket.tcpClient.ITcpClientListener;
import constrant.OpCode;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

public class ServerActionListioner implements ITcpClientListener {

    @BeanField
    private HandlerContextManager contextManager;

    private SocketHandlerRegistry handlerRegistry;
    private Peer peer;

    @Override
    public int getOpCode() {
        return OpCode.BROADCAST_MESSAGE_SERVER;
    }

    @Override
    public void onConnected(Peer peer) {
        System.out.println("connected at:"+peer.getRemoteAddress());
        this.peer=peer;
    }

    @Override
    public void onDisconnected() {
        System.out.println("disconnected at:"+peer.getRemoteAddress());
    }

    @Override
    public void onReceive(Peer peer, int opCode, ByteBuf byteBuf) throws Exception {
        int opcode=byteBuf.readUnsignedByte();
        AbstractSocketHandler handler= handlerRegistry.getHandler(opcode);
        HandlerContext handlerContext=contextManager.get(peer.getId());
        if(handler!=null){
            handler.handle(handlerContext,opcode,byteBuf,true);
        }else{
            System.out.println("Can't handle");
        }
    }
}
